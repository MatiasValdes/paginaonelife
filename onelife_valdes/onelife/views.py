from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.conf import settings
import json
from aplicacion.models import Actividad, Membresia, historial, Horario
from django.db.models import Count
from django.views.generic import View
from .utils import render_to_pdf

# home page.
def home(request):
    # variable de session num_visitas.
    num_visitas = request.session.get('num_visitas', 0)
    id_user = request.user.id
    request.session['num_visitas'] = num_visitas + 1
    
    # default template.
    template = loader.get_template('home.html')
    context = {
        'settings': settings,
        'dataarticulos': None,
        'num_visitas': num_visitas,
        'id_user': id_user,
    }
    # está autenticado?
    if (request.user.is_authenticated):
        
        # es superusuario?
        if (request.user.is_superuser):
            return HttpResponseRedirect('admin/')

        # pertene a cierto grupo?
        if (len(request.user.groups.all()) > 0):
            # revisar el grupo (el primero).
            grupo = request.user.groups.all()[0]
            
            # cambia el template.
            if (grupo.name == "Cliente"):
                template = loader.get_template('gimnasio/cliente/index.html')
            
            elif (grupo.name == "Administrador"):
                template = loader.get_template('gimnasio/administrador/index.html')
                
                    # otros grupos.
                pass
        else:
            # usuario sin grupo
            pass
    else:
        # usuario no autenticado.
        pass

    return HttpResponse(template.render(context, request)) 


