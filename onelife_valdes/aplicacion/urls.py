from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    # path('administrador/', views.index, name='index'),
    path('administrador/', views.horario, name='horario'),
    path('administrador/graficar/', views.graficar, name='graficar'),
    path('administrador/index', views.index2, name='index2'),
    path('administrador/pasados/', views.pasados, name='pasados'),
    path('administrador/usuario/', views.usuario, name='usuario'),
    path('administrador/usuariodashboard/', views.usuariodashboard, name='usuariodashboard'),
    path('administrador/usuarioadd/', views.usuarioadd, name='usuarioadd'),
    path('administrador/updateusuario/<int:id>', views.updateusuario, name='updateusuario'),
    path('administrador/deleteusuario/<int:id>', views.deleteusuario, name='deleteusuario'),
    path('administrador/usuariobuscar/<int:id>', views.usuariobuscar, name='usuariobuscar'),
    path('administrador/reservashoy/', views.reservashoy, name='reservashoy'),
    path('administrador/update2/<int:id>', views.update2, name='update2'),
    
    path('administrador/historial2/<int:id>', views.change_historial2, name='change_historial2'),
    path('administrador/historial3/<int:id>', views.change_historial3, name='change_historial3'),
    path('administrador/historial/', views.change_historial, name='change_historial'),
    path('administrador/update/<int:id>', views.update, name='update'),
    path('administrador/delete/<int:id>', views.delete, name='delete'),
    path('administrador/add/', views.add, name='add'),
    
    path('cliente/clienteindex', views.clienteindex, name='clienteindex'),
    path('cliente/membresia/', views.membresia, name='membresia'),
    path('cliente/historialhorario/', views.historialhorario, name='historialhorario'),

    path('musculacion/', views.musculacion, name='musculacion'),
    
    
    path('horas/', views.horas, name='horas'),
    path('horas/delete/<int:id>', views.delete_horas, name='delete'),

    path('reserva/', views.reserva, name='reserva'),

    path('reserva/update_res/<int:id>', views.reserva_upd, name='update_res'),
    path('registration/', views.Registro, name = 'registro'),
    ]
