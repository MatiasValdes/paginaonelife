from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import UsuarioCreationForm, UsuarioChangeForm
from .models import Usuario, Membresia, Bloque, Actividad, Profesor, Horario, historial

#
class UsuarioAdmin(UserAdmin):
    add_form = UsuarioCreationForm
    form = UsuarioChangeForm
    model = Usuario
    list_display = ["email", "username", "first_name", "last_name", 'telefono', 'rut']


class MembresiaAdmin(admin.ModelAdmin):
    list_display = ["tipo"]

class BloqueAdmin(admin.ModelAdmin):
    list_display = ["duracion"]

class ActividadAdmin(admin.ModelAdmin):
    list_display = ["nombre_act"]


class ProfesorAdmin(admin.ModelAdmin):
    list_display = ["nombre_prof", "apellido_prof"]

class HorarioAdmin(admin.ModelAdmin):
    list_display = ["fecha", "fk_bloque", "fk_actividad", "fk_profesor", "fk_usuario"]

class HistorialAdmin(admin.ModelAdmin):
    list_display = ["membresia", "usuario", "fecha_inicio", "fecha_termino", "estado"]


admin.site.register(Usuario, UsuarioAdmin)
admin.site.register(Membresia, MembresiaAdmin)
admin.site.register(Bloque, BloqueAdmin)
admin.site.register(Actividad, ActividadAdmin)
admin.site.register(Profesor, ProfesorAdmin)
admin.site.register(Horario, HorarioAdmin)
admin.site.register(historial, HistorialAdmin)
